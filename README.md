# Practica Blockchain

## CallerContract

## EthPriceOracle

## Orden de Ejecucion de Funciones y Archivos

1. Cree una instancia de `CallerContract.sol` y tome nota de su direccion.
2. Cree una instancia de `EthPriceOracle.sol`, envie en los parametros la direccion de la cuenta y tome nota de la direccion del contrato.
3. Ejecute la funcion `setOracleInstanceAddress()` y envie en los paramentos la direccion de la instancia del contrato `EthPriceOracle.sol` creado en el paso anterior.
4. Añada un oraculo mediante la funcion `addOracle()` de `EthPriceOracle.sol` y envie en los parametros la direccion de la cuenta.
5. Brinde un valor a THRESHOLD mediante la funcion `setThreshold()` y envie en los parametros el valor deseado.
6. Ejecute la funcion `updateEthPrice()` de `CallerContract.sol`. Dicha funcion a su vez ejecuta la funcion `getLatestEthPrice()` de `EthPriceOracle.sol`, tomar nota del id contenido en el evento `GetLatestEthPriceEvent` emitido.
7. Ejecute la funcion `setLatestEthPrice()` de `EthPriceOracle.sol` y envie en los parametros el precio del ETH, la direccion de la instancia del contrato `CallerContract.sol` creada en el paso 1 y el id obtenido en el paso anterior.
