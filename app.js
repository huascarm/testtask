var express = require('express');
var app = express();
var errorhandler = require('errorhandler');
var passport = require('passport');
var bodyParser = require('body-parser');
var priceUpdater = require('./controllers/priceUpdater.controller');

require('dotenv').config();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(errorhandler());

require('./models/reporter.model');
require('./config/passport');

app.use(require('./routes'));

/// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    errors: {
      message: err.message,
      error: {},
    },
  });
});

var server = app.listen(process.env.PORT || 3000, function () {
  priceUpdater.init();
  console.log('Listening on port ' + server.address().port);
});
