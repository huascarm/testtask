var router = require('express').Router();
var passport = require('passport');
var reporterController = require('../../controllers/reporter.controller');
var web3Controller = require('../../controllers/web3.controller');

var jwtController = require('../../controllers/jwt.controller');

var auth = require('../auth');

router.post('/oauth/authorization', function (req, res, next) {
  const { label, key } = req.body;
  if (!label) {
    return res.status(400).json({ errors: { label: "can't be blank" } });
  }

  if (!key) {
    return res.status(400).json({ errors: { key: "can't be blank" } });
  }

  var _reporter = reporterController.findByLabel(req.body.label);
  if (_reporter.key != req.body.key) {
    return res.status(400).json({ errors: { password: 'credentials error' } });
  }
  var token = jwtController.generateJWT(_reporter);
  return res.json({ token });
});

router.get('/getAveragePrice', auth.required, async function (req, res, next) {
  var _averagePrice = await web3Controller.getAveragePrice();
  return res.json(_averagePrice);
});

router.post('/setEthPrice', auth.required, async function (req, res, next) {
  const { label, price } = req.body;
  if (!label) {
    return res.status(400).json({ errors: { label: "can't be blank" } });
  }

  if (!price) {
    return res.status(400).json({ errors: { price: "can't be blank" } });
  }

  var _pending = await web3Controller.setEthPrice(label, price);
  return res.json(_pending);
});

module.exports = router;
