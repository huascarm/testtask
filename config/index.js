var fs = require('fs');
var file = fs.readFileSync(process.env.USERS);
var users = JSON.parse(file);

module.exports = {
    secret: process.env.NODE_ENV === 'production' ? process.env.SECRET : 'secret',
    users: process.env.USERS ? users : []
};