// SPDX-License-Identifier: MIT
pragma solidity >=0.4.21 <0.7.0;
contract CallerContractInterface {
  function callback(uint256 _ethPrice, uint256 id) public;
}
