// SPDX-License-Identifier: MIT
pragma solidity >=0.4.21 <0.7.0;
contract EthPriceOracleInterface {
  function getLatestEthPrice() public returns (uint256);
}