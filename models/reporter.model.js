class Reporter {
    label = null;
    price = null;
    key = null;
    constructor() {}

    setPrice(_price) {
        this.price = _price;
    }

    getPrice() {
        return this.price;
    }

    setKey(_key) {
        this.key = _key;
    }

    getKey() {
        return this.key;
    }
    
    setLabel(_label) {
        this.label = _label;
    }

    getLabel() {
        return this.label;
    }

}
module.exports = Reporter;