var jwt = require('jsonwebtoken');
var secret = require('../config').secret;
class JWTController {
    generateJWT(reporter) {
        var today = new Date();
        var exp = new Date(today);
        exp.setDate(today.getDate() + 60);
        console.log(parseInt(exp.getTime() / 1000));
        return jwt.sign({
          id: reporter.key,
          username: reporter.label,
          exp: parseInt(exp.getTime() / 1000),
        }, secret);
    };
}

module.exports = new JWTController()