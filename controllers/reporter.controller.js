var users = require('../config/index').users;

class ReporterController {
  findByLabel(_label) {
    return users.find((user) => user.label == _label);
  }

  find() {
    return 'hello find';
  }
}

module.exports = new ReporterController();
