const SLEEP_INTERVAL = process.env.SLEEP_INTERVAL || 2000;
const OWNER_ADDRESS =
  process.env.OWNER_ADDRESS || '0xdA0b3156C20Da8c1D8df68284b14C23abe102b06';
const axios = require('axios');
const web3Controller = require('./web3.controller');
const CHUNK_SIZE = process.env.CHUNK_SIZE || 3;
const MAX_RETRIES = process.env.MAX_RETRIES || 5;
const BN = require('bn.js');

class priceUpdater {
  async init() {
    this.pendingRequests = [];
    const oracleContract = await web3Controller.getOracle();
    this.filterEvents(oracleContract);

    setInterval(async () => {
      await this.processQueue(oracleContract, OWNER_ADDRESS);
    }, SLEEP_INTERVAL);
  }

  async filterEvents(oracleContract) {
    await oracleContract.events
      .GetLatestEthPriceEvent({})
      .on('data', (event) => {
        console.log('caro !', event);
        this.addRequestToQueue(event);
      });
  }
  async addRequestToQueue(event) {
    const callerAddress = event.returnValues.callerAddress;
    const id = event.returnValues.id;
    this.pendingRequests.push({ callerAddress, id });
  }

  async processQueue(oracleContract, ownerAddress) {
    let processedRequests = 0;
    while (this.pendingRequests.length > 0 && processedRequests < CHUNK_SIZE) {
      const req = this.pendingRequests.shift();
      await processRequest(
        oracleContract,
        ownerAddress,
        req.id,
        req.callerAddress
      );
      processedRequests++;
    }
  }

  async processRequest(oracleContract, ownerAddress, id, callerAddress) {
    let retries = 0;
    while (retries < MAX_RETRIES) {
      try {
        const ethPrice = await this.retrieveLatestEthPrice();
        await this.setLatestEthPrice(
          oracleContract,
          callerAddress,
          ownerAddress,
          ethPrice,
          id
        );
        return;
      } catch (error) {
        if (retries === MAX_RETRIES - 1) {
          await this.setLatestEthPrice(
            oracleContract,
            callerAddress,
            ownerAddress,
            '0',
            id
          );
          return;
        }
        retries++;
      }
    }
  }

  async retrieveLatestEthPrice() {
    const resp = await axios({
      url: 'https://api.binance.com/api/v3/ticker/price',
      params: {
        symbol: 'ETHUSDT',
      },
      method: 'get',
    });
    return resp.data.price;
  }

  async setLatestEthPrice(
    oracleContract,
    callerAddress,
    ownerAddress,
    ethPrice,
    id
  ) {
    ethPrice = ethPrice.replace('.', '');
    const multiplier = new BN(10 ** 10, 10);
    const ethPriceInt = new BN(parseInt(ethPrice), 10).mul(multiplier);
    const idInt = new BN(parseInt(id));
    try {
      await oracleContract.methods
        .setLatestEthPrice(
          ethPriceInt.toString(),
          callerAddress,
          idInt.toString()
        )
        .send({ from: ownerAddress });
    } catch (error) {
      console.log('Error encountered while calling setLatestEthPrice.');
    }
  }
}

module.exports = new priceUpdater();
