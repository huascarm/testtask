const Web3 = require('web3');
const Oracle = require('../build/contracts/EthPriceOracle.json');

class web3Controller {
  async getOracle() {
    const web3 = new Web3('http://localhost:9545');
    const id = await web3.eth.net.getId();
    const deployedNetwork = Oracle.networks[id];
    const oracleContract = new web3.eth.Contract(
      Oracle.abi,
      deployedNetwork.address
    );
    return oracleContract;
  }

  async getAveragePrice() {
    console.log(this.contract);
    return 'average price';
  }

  async setEthPrice(label, price) {
    return { label, price };
  }
}

module.exports = new web3Controller();
